const path = require(`path`);

module.exports = {
  webpack: {
    alias: {
      '@api': path.join(path.resolve(__dirname, './src/api')),
      '@constants': path.join(path.resolve(__dirname, './src/constants')),
      '@components': path.join(path.resolve(__dirname, './src/components')),
      '@config': path.join(path.resolve(__dirname, './src/config')),
      '@context': path.join(path.resolve(__dirname, './src/context')),
      '@hooks': path.join(path.resolve(__dirname, './src/hooks')),
      '@images': path.join(path.resolve(__dirname, './src/images')),
      '@sharedStyles': path.join(path.resolve(__dirname, './src/sharedStyles')),
      '@utils': path.join(path.resolve(__dirname, './src/utils'))
    }
  }
};
