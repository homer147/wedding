import { Endpoints } from './constants';
import { ContactFormState } from '@components/Contact';
import { SubmitCodeBody, SubmitInfoBody } from '@components/Rsvp';

const baseUrl = process.env.REACT_APP_API_BASE;
const { GETRSVPDETAILS, SUBMITCONTACTFORM, UPDATERSVPDETAILS } = Endpoints;

type ApiPromise = {
  errorCode?: number;
  message?: string;
  data?: {
    [key: string]: string | boolean | number;
  };
};

// TODO - any
const call = async (url: string, opts: any): Promise<any> => {
  return fetch(url, opts)
    .then((data) => {
      return data;
    })
    .catch((err) => {
      return {
        status: 500,
        message: err
      };
    });
};

export const submitRsvpCode = async (code: SubmitCodeBody): Promise<ApiPromise> => {
  const url = `${baseUrl}${GETRSVPDETAILS}`;

  const res = await call(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ code })
  });

  if (res.ok) {
    return await res.json();
  }

  return { errorCode: res.status };
};

export const submitRsvpInfo = async (info: SubmitInfoBody): Promise<ApiPromise> => {
  const url = `${baseUrl}${UPDATERSVPDETAILS}`;

  const res = await call(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ info })
  });

  if (res.ok) {
    return await res.json();
  }

  return { errorCode: res.status, message: res.message };
};

export const submitContactForm = async (formData: ContactFormState): Promise<ApiPromise> => {
  const url = `${baseUrl}${SUBMITCONTACTFORM}`;

  const res = await call(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ formData })
  });

  if (res.ok) {
    return { data: { sent: true } };
  }

  return { errorCode: res.status };
};
