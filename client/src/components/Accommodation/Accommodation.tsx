import React from 'react';
import styled from 'styled-components';

import { Hero, Map } from '@components';
import image from '@images/veriu-central.jpg';
import { hotelCoords } from '@constants';
import { Container, Content, List, ListItem, Paragraph } from '@sharedStyles';

const BoldSpan = styled.span`
  font-weight: bold;
`;

const Accommodation = () => {
  return (
    <Container>
      <Hero image={image} />
      <Content>
        <List>
          <ListItem>Veriu Central</ListItem>
          <ListItem>75 Wentworth Ave, Sydney NSW 2000</ListItem>
        </List>
        <Paragraph>
          Only 15 minutes walk from the Ceremony venue and 10 minutes walk from central station,
          Veriu is the perfect place to stay for anyone attending our wedding. And the team at Veriu
          have put together an exclusive offer for our guests!
        </Paragraph>
        <Paragraph>
          Anyone attending our wedding will receive 15% off the 'best available rate' &amp; late
          check out of 14:00. If you are able to find a 3rd party website has an even cheaper price
          advertised, Veriu will match it and still throw in the late check-out.
        </Paragraph>
        <Paragraph>
          Please contact Groups Coordinator: <BoldSpan>Alexis Ruiz</BoldSpan> directly to organize
          your discounted accommodation.
        </Paragraph>
        <Paragraph>
          Email: groups@punthill.com.au
          <br />
          Contact number: 1300 964 821
          <br />
          Hours: Mon-Fri 08:00 – 16:00
        </Paragraph>
        <Paragraph>Offsite car parking is available for $35.00 per day</Paragraph>
        <Map coordinates={hotelCoords} />
      </Content>
    </Container>
  );
};

export default Accommodation;
