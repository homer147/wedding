import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import styled from 'styled-components';

import { Accommodation, Ceremony, Contact, Header, Home, Reception, Registry } from '@components';
import { remify } from '@utils';

const Container = styled.div`
  min-height: 100vh;
  padding-bottom: ${remify(30)};
  background-color: ${({ theme }) => theme.colors.hunterGreen};
  box-sizing: border-box;
`;

function App() {
  console.info('*** Yes, I did make this ***');

  return (
    <Container>
      <Router>
        <Header />
        <Switch>
          <Route path="/accommodation">
            <Accommodation />
          </Route>
          <Route path="/ceremony">
            <Ceremony />
          </Route>
          <Route path="/reception">
            <Reception />
          </Route>
          <Route path="/gifts">
            <Registry />
          </Route>
          <Route path="/contact">
            <Contact />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </Container>
  );
}

export default App;
