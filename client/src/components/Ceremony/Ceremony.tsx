import React from 'react';

import { Hero, Map } from '@components';
import image from '@images/the-mint.jpg';
import { ceremonyCoords } from '@constants';
import { Container, Content, List, ListItem, Paragraph } from '@sharedStyles';

const Ceremony = () => {
  return (
    <Container>
      <Hero image={image} />
      <Content>
        <List>
          <ListItem>10 Macquarie Street, Sydney NSW 2000</ListItem>
          <ListItem>Arrive from 2pm for a 2:30pm start</ListItem>
        </List>
        <Paragraph>
          Initially part of a hospital for convicts under Governor Macquarie from 1816, the south
          wing became the first overseas branch of the Royal Mint in London in 1854. Today, The Mint
          is the head office of Sydney Living Museums and our ceremony will be held in its beautiful
          courtyard.
        </Paragraph>
        <Map coordinates={ceremonyCoords} />
      </Content>
    </Container>
  );
};

export default Ceremony;
