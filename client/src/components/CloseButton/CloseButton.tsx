import React from 'react';
import styled from 'styled-components';

import { CloseIcon } from '@sharedStyles';

const Container = styled.button`
  position: absolute;
  top: 0;
  right: 0;
  padding: 0;
  background: transparent;
  border: none;
  color: ${({ theme }) => theme.colors.ivory};
`;

interface CloseButtonProps {
  onClick: () => void;
}

const CloseButton = ({ onClick }: CloseButtonProps) => {
  const handleKeydown = (e: React.KeyboardEvent) => {
    if (e.shiftKey && e.key === 'Tab') {
      e.preventDefault();
    }
  };

  return (
    <Container onClick={onClick} onKeyDown={handleKeydown}>
      <CloseIcon />
    </Container>
  );
};

export default CloseButton;
