import React, { useContext } from 'react';
import styled from 'styled-components';

import { submitContactForm } from '@api';
import { Form, Message } from '@components';
import formData from '@config/contactForm.json';
import MessagesData from '@config/messages.json';
import { ModalContext } from '@context/modalContext';
import { ButtonLabels, callResult } from '@constants';
import { Container, Content, ContentHeading } from '@sharedStyles';
import { remify } from '@utils';

export type ContactFormState = {
  senderEmailAddress: string;
  message: string;
  captchaCode: string;
  senderName: string;
  senderPhone: string;
};

const FormContainer = styled.div`
  max-width: ${remify(500)};
  margin: ${remify(30)} auto 0;
`;

const Contact = () => {
  const { openModal } = useContext(ModalContext);

  const submitForm = (finaliseCall: (success: boolean) => void) => async (
    state: any // TODO - any
  ) => {
    const { email, message, name, recaptcha, phone } = state;

    const body = {
      senderEmailAddress: email.value,
      message: message.value,
      captchaCode: recaptcha,
      senderName: name.value,
      senderPhone: phone.value
    };

    const res = await submitContactForm(body);

    if (res.errorCode === 400) {
      finaliseCall(callResult.fail);
      openModal(<Message message={MessagesData.captchaError} />);
      console.error(`[Contact] ${res.message}`);

      return;
    }

    if (res.errorCode) {
      finaliseCall(callResult.fail);
      openModal(<Message message={MessagesData.serverError} />);
      console.error(`[Contact] ${res.message}`);

      return;
    }

    finaliseCall(callResult.success);
    openModal(<Message message={MessagesData.messageSent} />);
  };

  return (
    <Container>
      <Content>
        <ContentHeading>Contact Us</ContentHeading>
        <FormContainer>
          <Form
            inputs={formData}
            onSubmit={submitForm}
            submitLabel={ButtonLabels.SUBMIT}
            recaptcha
          />
        </FormContainer>
      </Content>
    </Container>
  );
};

export default Contact;
