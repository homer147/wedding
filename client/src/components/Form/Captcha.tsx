import React, { forwardRef, useEffect, useRef } from 'react';
import styled from 'styled-components';
import ReCAPTCHA from 'react-google-recaptcha';

import { captchaSiteKey } from '@constants';
import usePrevious from '@hooks/usePrevious';
import { remify } from '@utils';

export const captchaKey = 'recaptcha';

interface CaptchaProps {
  errorMessage?: string;
  onChange: (value: string | null) => void;
  ref: any; // TODO - any
  showTooltip: (ref: HTMLDivElement, key: string) => void;
}

const CaptchaContainer = styled.div`
  margin: ${remify(10)} 0 ${remify(35)};
`;

const Captcha = forwardRef(({ errorMessage, onChange, showTooltip }: CaptchaProps, ref: any) => {
  const containerRef = useRef<HTMLDivElement>(null);
  const prevError = usePrevious(errorMessage);

  useEffect(() => {
    if (errorMessage && errorMessage !== prevError) {
      showTooltip(containerRef.current!, captchaKey);
    }
  }, [errorMessage, prevError, showTooltip]);

  return (
    <CaptchaContainer data-tip={errorMessage} ref={containerRef}>
      <ReCAPTCHA onChange={onChange} ref={ref} sitekey={captchaSiteKey} />
    </CaptchaContainer>
  );
});

export default Captcha;
