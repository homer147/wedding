import React, { useCallback, useReducer, useRef } from 'react';
import styled from 'styled-components';
import ReactTooltip from 'react-tooltip';

import { getDependencyOptions, remify } from '@utils';
import messages from '@config/messages.json';
import { StyledButton } from '@sharedStyles';
import Captcha, { captchaKey } from './Captcha';
import Input from './Input';
import reducer, {
  TAction,
  TInputsState,
  TFormState,
  finaliseCallAction,
  submitPendingAction,
  updateCaptchaAction,
  updateErrorAction,
  updateInputAction
} from './reducer';

export type TValidation = {
  message: string;
  rule: string;
};

export type TInput = {
  key?: string;
  inputKey?: string;
  label?: string;
  options?: string[];
  notReqIf?: string[];
  required?: boolean;
  touched?: boolean;
  type?: string;
  value?: string;
  validations?: TValidation[];
};

interface FormProps {
  inputs: TInput[];
  onSubmit: (
    cb: (success: boolean) => void
  ) => (state: TInputsState, rowNumber: number | undefined) => void;
  recaptcha?: boolean;
  rowNumber?: number;
  submitLabel?: string;
}

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 ${remify(15)};
`;

export const initialState: TFormState = {
  inputs: {},
  errorMessages: {},
  pending: false,
  recaptcha: null
};

const Form = ({
  inputs,
  onSubmit,
  recaptcha = false,
  rowNumber,
  submitLabel = 'Submit'
}: FormProps) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const buttonRef = useRef<HTMLButtonElement>(null);
  const formRef = useRef<HTMLFormElement>(null);
  const tooltipRef = useRef<string | null>(null);
  const timeoutRef = useRef<ReturnType<typeof setTimeout>>();
  const captchaRef = useRef<any>(null); // TODO - any

  const updateState = useCallback((inputKey: string, inputState: TInput) => {
    const action = {
      type: updateInputAction,
      payload: { inputKey, inputState }
    };

    dispatch(action);
  }, []);

  const updateErrorMessage = (inputKey: string, message: string) => {
    if (state.pending) {
      return;
    }

    const action = {
      type: updateErrorAction,
      payload: { inputKey, message }
    };

    dispatch(action);
  };

  const updateCaptchaCode = (value: string | null) => {
    if (value === state.recaptcha) {
      return;
    }

    const action = {
      type: updateCaptchaAction,
      payload: value
    };

    dispatch(action);
  };

  const finaliseCall = (success: boolean) => {
    const action: TAction = {};

    if (success) {
      action.type = finaliseCallAction;
      captchaRef.current.reset();
    } else {
      action.type = submitPendingAction;
      action.payload = false;
    }

    dispatch(action);
  };

  const dependencyHasValue = (inputKey: string) => {
    const inputState = state.inputs[inputKey];

    return inputState && !!inputState.value;
  };

  const getErrorMessage = (validations: TValidation[], inputState: TInput): string => {
    validations.forEach(({ rule, message }) => {
      const pattern = new RegExp(rule);
      const { value } = inputState;

      if (!value || !pattern.test(value)) {
        return message;
      }
    });

    return '';
  };

  const validateForm = () => {
    const { errorMessages, inputs: stateInputs, recaptcha: recaptchaState } = state;
    const actionArr: TAction[] = [];
    let isFormValid = (formRef.current && formRef.current.checkValidity()) || false;

    inputs.forEach(({ key, notReqIf, required, validations }) => {
      if (key && !errorMessages[key]) {
        const inputState = stateInputs[key] || {};

        if (!inputState.touched) {
          const action: TAction = {
            type: updateInputAction,
            payload: {
              inputKey: key,
              inputState: { touched: true }
            }
          };

          actionArr.push(action);
        }

        if (required && (!stateInputs[key] || !stateInputs[key].value)) {
          isFormValid = false;
          updateErrorMessage(key, 'This field is required');
        }

        if (validations) {
          let errorMessage;

          if (notReqIf) {
            notReqIf.forEach((dependency) => {
              if (notReqIf && stateInputs[key!] && stateInputs[key!].value) {
                notReqIf.forEach((dependency) => {
                  updateErrorMessage(dependency, '');
                });

                errorMessage = getErrorMessage(validations, inputState);
              } else if (!dependencyHasValue(dependency)) {
                errorMessage = `Please provide either ${getDependencyOptions(key, notReqIf)}`;
              }
            });
          } else if (!isFormValid) {
            errorMessage = getErrorMessage(validations, inputState);
          }

          errorMessage && updateErrorMessage(key!, errorMessage);
        }
      }
    });

    if (recaptcha && isFormValid && !recaptchaState) {
      updateErrorMessage(captchaKey, messages.captchaError);
      isFormValid = false;
    }

    Promise.all(actionArr.map((action) => dispatch(action)));

    return isFormValid;
  };

  const handleSubmit = (e: React.SyntheticEvent) => {
    const { inputs, recaptcha } = state;
    const isFormValid = validateForm();
    const pendingAction: TAction = {
      type: submitPendingAction,
      payload: true
    };

    dispatch(pendingAction);
    e.preventDefault();
    buttonRef.current && buttonRef.current.focus();

    if (!isFormValid) {
      pendingAction.payload = false;
      dispatch(pendingAction);

      return;
    }

    onSubmit(finaliseCall)({ ...inputs, recaptcha }, rowNumber);
  };

  const handleKeydown = (e: React.KeyboardEvent) => {
    if (!e.shiftKey && e.key === 'Tab') {
      e.preventDefault();
    }
  };

  const stopTimer = () => {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
      timeoutRef.current = undefined;
    }
  };

  const hideTooltip = (inputKey: string) => {
    if (inputKey === tooltipRef.current) {
      if (tooltipRef.current === captchaKey) {
        updateErrorMessage(captchaKey, '');
      }

      ReactTooltip.hide();
      stopTimer();
      tooltipRef.current = null;
    }
  };

  const showTooltip = (ref: Element, inputKey: string) => {
    ReactTooltip.show(ref);
    stopTimer();

    timeoutRef.current = setTimeout(() => {
      if (tooltipRef.current) {
        hideTooltip(tooltipRef.current);
      }
    }, 3000);

    tooltipRef.current = inputKey;
  };

  return (
    <StyledForm ref={formRef}>
      {inputs.map(
        (
          { key = '', label, options, notReqIf, required, type = 'text', validations }: TInput,
          idx: number
        ) => (
          <Input
            autoFocus={idx === 0}
            errorMessage={state.errorMessages[key]}
            key={key}
            hideTooltip={hideTooltip}
            inputKey={key || idx.toString()}
            options={options}
            label={label}
            notReqIf={notReqIf}
            required={required}
            setErrorMessage={updateErrorMessage}
            setInputValue={updateState}
            showTooltip={showTooltip}
            touched={state.inputs[key]?.touched}
            type={type}
            dependencyHasValue={dependencyHasValue}
            validations={validations}
            value={state.inputs[key]?.value}
          />
        )
      )}
      {recaptcha && (
        <Captcha
          errorMessage={state.errorMessages.recaptcha}
          onChange={updateCaptchaCode}
          ref={captchaRef}
          showTooltip={showTooltip}
        />
      )}
      <StyledButton
        disabled={state.pending}
        ref={buttonRef}
        onClick={handleSubmit}
        onKeyDown={handleKeydown}
      >
        {submitLabel}
      </StyledButton>
      <ReactTooltip place="bottom" type="error" />
    </StyledForm>
  );
};

export default Form;
