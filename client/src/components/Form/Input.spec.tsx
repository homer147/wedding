import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'styled-components';

import Input from './Input';

const theme = {
  fontSizes: {
    small: 12
  }
};

describe('Input', () => {
  const defaultProps = {
    inputKey: 'code',
    setErrorMessage: jest.fn(),
    setInputValue: jest.fn(),
    type: 'text'
  };

  const doWithMode = (mode: (component: JSX.Element) => any) => (
    overrides = {}
  ) =>
    mode(
      <ThemeProvider theme={theme}>
        <Input {...defaultProps} {...overrides} />
      </ThemeProvider>
    );

  it('renders without crashing', () => {
    const Input = doWithMode(mount)();
    expect(Input).toMatchSnapshot();
  });

  it('renders a text input', () => {
    const Input = doWithMode(mount)();
    const elem = Input.find('input').first();

    expect(elem.exists()).toBe(true);
  });

  it('renders a select input', () => {
    const Input = doWithMode(mount)({
      inputKey: 'maxAdults',
      type: 'select'
    });
    const elem = Input.find('select').first();

    expect(elem.exists()).toBe(true);
  });
});
