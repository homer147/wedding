import React from 'react';
import styled from 'styled-components';

import { TInput, TValidation } from './Form';
import InputSelect from './InputSelect';
import InputText from './InputText';
import InputTextarea from './InputTextarea';
import { FormConstants } from '@constants';
import { remify } from '@utils';

export interface InputProps {
  autoFocus?: boolean;
  errorMessage?: string;
  hideTooltip?: (inputKey: string) => void;
  inputKey: string;
  label?: string;
  options?: string[];
  notReqIf?: string[];
  required?: boolean;
  setErrorMessage: (inputKey: string, message: string) => void;
  setInputValue: (inputKey: string, state: TInput) => void;
  showTooltip?: (ref: Element, inputKey: string) => void;
  touched?: boolean;
  type?: string;
  dependencyHasValue?: (dependency: string) => boolean;
  validations?: TValidation[];
  value?: string;
}

export type TStyledInput = {
  errorMessage?: string;
  touched?: boolean;
};

const Container = styled.span`
  width: 100%;
  text-align: center;
`;

export const StyledLabel = styled.label`
  display: block;
  font-size: ${({ theme }) => theme.fontSizes.small};
  width: 100%;
  margin-bottom: ${remify(5)};
`;

const { EMAIL, NUMBER, SELECT, TEXT, TEXTAREA } = FormConstants;
const Input = (props: InputProps) => {
  const { type } = props;

  return (
    <Container>
      {(type === TEXT || type === NUMBER || type === EMAIL) && <InputText {...props} />}
      {type === SELECT && <InputSelect {...props} />}
      {type === TEXTAREA && <InputTextarea {...props} />}
    </Container>
  );
};

export default Input;
