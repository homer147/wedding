import React from 'react';
import { mount, render } from 'enzyme';
import { ThemeProvider } from 'styled-components';

import InputSelect from './InputSelect';

const theme = {
  colors: {
    error: 'red'
  },
  fontSizes: {
    small: 12
  }
};

describe('InputSelect', () => {
  const defaultProps = {
    hideTooltip: jest.fn(),
    inputKey: 'maxAdults',
    setErrorMessage: jest.fn(),
    setInputValue: jest.fn()
  };

  const doWithMode = (mode: (component: JSX.Element) => any) => (
    overrides = {}
  ) =>
    mode(
      <ThemeProvider theme={theme}>
        <InputSelect {...defaultProps} {...overrides} />
      </ThemeProvider>
    );

  it('renders without crashing', () => {
    const InputSelect = doWithMode(render)();
    expect(InputSelect).toMatchSnapshot();
  });

  it('calls setInputValue when onChange is fired', () => {
    const InputSelect = doWithMode(mount)();
    const { inputKey, setInputValue } = defaultProps;
    const value = 'spam';
    const event = { target: { value } };

    InputSelect.find('select').simulate('change', event);

    expect(setInputValue).toBeCalledWith(inputKey, { value });
  });

  it('sets input state as touched on blur', () => {
    const InputSelect = doWithMode(mount)();
    const { inputKey, setInputValue } = defaultProps;

    InputSelect.find('select').simulate('blur', {});

    expect(setInputValue).toBeCalledWith(inputKey, { touched: true });
  });

  it('does not set input state as touched on blur when already set', () => {
    const InputSelect = doWithMode(mount)({
      touched: true
    });
    const { setInputValue } = defaultProps;

    InputSelect.find('select').simulate('blur', {});

    expect(setInputValue).toBeCalledTimes(0);
  });

  it('resets error and hides tooltip when in focus', () => {
    const InputSelect = doWithMode(mount)({
      errorMessage: 'error'
    });
    const { hideTooltip, inputKey, setErrorMessage } = defaultProps;
    InputSelect.find('select').simulate('focus', {});

    expect(setErrorMessage).toBeCalledWith(inputKey, '');
    expect(hideTooltip).toBeCalledTimes(1);
  });
});
