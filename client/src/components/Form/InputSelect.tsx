import React, { memo, useEffect, useRef } from 'react';
import styled from 'styled-components';

import { InputProps, StyledLabel, TStyledInput } from './Input';
import usePrevious from '@hooks/usePrevious';
import { SharedInputStyles } from '@sharedStyles';
import { noop, remify } from '@utils';

const StyledSelect = styled.select<TStyledInput>`
  ${SharedInputStyles}

  ${({ errorMessage, theme, touched }) =>
    errorMessage && touched && `box-shadow: inset 0px 0px 0px ${remify(2)} ${theme.colors.error};`}
`;

const InputSelect = ({
  autoFocus,
  errorMessage,
  hideTooltip = noop,
  inputKey,
  label,
  options,
  required,
  setErrorMessage,
  setInputValue,
  showTooltip = noop,
  touched,
  value = ''
}: InputProps) => {
  const inputRef = useRef<HTMLSelectElement>(null);
  const focusRef = useRef<boolean>(false);
  const prevError = usePrevious(errorMessage);

  const validate = () => {
    const validationMessage = inputRef.current!.validationMessage || '';

    if (validationMessage) {
      setErrorMessage(inputKey, validationMessage);
    }
  };

  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setInputValue(inputKey, { value: e.target.value });
  };

  const handleBlur = () => {
    focusRef.current = false;
    validate();

    if (!touched) {
      setInputValue(inputKey, { touched: true });
    }
  };

  const handleFocus = () => {
    focusRef.current = true;

    if (errorMessage) {
      setErrorMessage(inputKey, '');
      inputRef.current!.setCustomValidity('');
    }

    hideTooltip(inputKey);
  };

  useEffect(() => {
    const validationMessage = inputRef.current!.validationMessage || '';

    if (errorMessage && errorMessage !== prevError) {
      inputRef.current!.setCustomValidity(errorMessage);
      showTooltip(inputRef.current!, inputKey);
    }

    if (touched && !focusRef.current && validationMessage && validationMessage !== errorMessage) {
      setErrorMessage(inputKey, validationMessage);
    }
  }, [errorMessage, inputKey, prevError, setErrorMessage, showTooltip, touched]);

  return (
    <React.Fragment>
      <StyledLabel>{`${label}${required ? '*' : ''}`}</StyledLabel>
      <StyledSelect
        autoFocus={autoFocus}
        data-tip={errorMessage}
        errorMessage={errorMessage}
        onBlur={handleBlur}
        onChange={handleChange}
        onFocus={handleFocus}
        ref={inputRef}
        required={required}
        touched={touched}
        value={value}
      >
        <option value="" disabled>
          {' '}
          -- select an option --{' '}
        </option>
        {options?.map((option: string) => (
          <option value={option} key={option}>
            {option}
          </option>
        ))}
      </StyledSelect>
    </React.Fragment>
  );
};

export default memo(InputSelect);
