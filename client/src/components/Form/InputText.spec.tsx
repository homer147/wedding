import React from 'react';
import { mount, render } from 'enzyme';
import { ThemeProvider } from 'styled-components';

import InputText from './InputText';

const theme = {
  colors: {
    error: 'red'
  },
  fontSizes: {
    small: 12
  }
};

describe('InputText', () => {
  const defaultProps = {
    errorMessage: '',
    inputKey: 'code',
    hideTooltip: jest.fn(),
    setErrorMessage: jest.fn(),
    setInputValue: jest.fn(),
    showTooltip: jest.fn(),
    touched: false,
    validations: [
      {
        message: 'error',
        rule: '^[0-9]{6}$'
      }
    ]
  };

  const doWithMode = (mode: (component: JSX.Element) => any) => (
    overrides = {}
  ) =>
    mode(
      <ThemeProvider theme={theme}>
        <InputText {...defaultProps} {...overrides} />
      </ThemeProvider>
    );

  it('renders without crashing', () => {
    const inputText = doWithMode(render)();
    expect(inputText).toMatchSnapshot();
  });

  it('calls setInputValue when onChange has fired', () => {
    const inputText = doWithMode(mount)();
    const { inputKey, setInputValue } = defaultProps;
    const value = 'spam';
    const event = { target: { value } };

    inputText.find('input').simulate('change', event);

    expect(setInputValue).toBeCalledWith(inputKey, { value });
  });

  it('sets input state as touched on blur', () => {
    const inputText = doWithMode(mount)();
    const { inputKey, setInputValue } = defaultProps;

    inputText.find('input').simulate('blur', {});

    expect(setInputValue).toBeCalledWith(inputKey, { touched: true });
  });

  it('does not set input state as touched on blur when already set', () => {
    const inputText = doWithMode(mount)({
      touched: true
    });
    const { setInputValue } = defaultProps;

    inputText.find('input').simulate('blur', {});

    expect(setInputValue).toBeCalledTimes(0);
  });

  it('calls setErrorMessage when validation fails on blur', () => {
    const inputText = doWithMode(mount)();
    const { inputKey, setErrorMessage, validations } = defaultProps;
    const value = 'spam';
    const event = { target: { value } };

    inputText.find('input').simulate('blur', event);

    expect(setErrorMessage).toBeCalledWith(inputKey, validations[0].message);
  });

  it('does not call setErrorMessage when validation passes on blur', () => {
    const inputText = doWithMode(mount)();
    const { setErrorMessage } = defaultProps;
    const value = '101010';
    const event = { target: { value } };

    inputText.find('input').simulate('blur', event);

    expect(setErrorMessage).toBeCalledTimes(0);
  });

  it('resets error and hides tooltip when in focus', () => {
    const inputText = doWithMode(mount)({
      errorMessage: 'error'
    });
    const { hideTooltip, inputKey, setErrorMessage } = defaultProps;
    inputText.find('input').simulate('focus', {});

    expect(setErrorMessage).toBeCalledWith(inputKey, '');
    expect(hideTooltip).toBeCalledTimes(1);
  });
});
