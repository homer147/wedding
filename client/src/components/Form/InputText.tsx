import React, { memo, useEffect, useRef } from 'react';
import styled from 'styled-components';

import { InputProps, StyledLabel, TStyledInput } from './Input';
import usePrevious from '@hooks/usePrevious';
import { SharedInputStyles } from '@sharedStyles';
import { executeValidationRules, getDependencyOptions, noop, remify } from '@utils';

const StyledInput = styled.input<TStyledInput>`
  ${SharedInputStyles}
  -moz-appearance: textfield;

  ::-webkit-outer-spin-button,
  ::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  ${({ errorMessage, theme, touched }) =>
    errorMessage && touched && `box-shadow: inset 0px 0px 0px ${remify(2)} ${theme.colors.error};`}
`;

const InputText = ({
  autoFocus,
  errorMessage,
  hideTooltip = noop,
  inputKey,
  label,
  notReqIf,
  required,
  setErrorMessage = noop,
  setInputValue = noop,
  showTooltip = noop,
  touched,
  type,
  dependencyHasValue,
  validations = [],
  value = ''
}: InputProps) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const prevError = usePrevious(errorMessage);

  const validate = (e: React.FocusEvent<HTMLInputElement>) => {
    let errorMessage = inputRef.current!.validationMessage || '';

    if (notReqIf && e.target.value) {
      notReqIf.forEach((dependency) => {
        setErrorMessage(dependency, '');
      });
    }

    if (!e.target.value && notReqIf && !errorMessage) {
      for (let i = 0; i < notReqIf.length; i++) {
        const dependency = notReqIf[i];

        if (dependencyHasValue && !dependencyHasValue(dependency)) {
          if (!errorMessage) {
            errorMessage = `Please provide either ${getDependencyOptions(inputKey, notReqIf)}`;
          }
        } else {
          errorMessage = '';

          return;
        }
      }
    }

    if (!errorMessage) {
      errorMessage = executeValidationRules(validations, e.target.value) || '';
    }

    if (errorMessage) {
      setErrorMessage(inputKey, errorMessage);
    }
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(inputKey, { value: e.target.value });
  };

  const handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    validate(e);

    if (!touched) {
      setInputValue(inputKey, { touched: true });
    }
  };

  const handleFocus = () => {
    if (errorMessage) {
      setErrorMessage(inputKey, '');
      inputRef.current!.setCustomValidity('');
    }

    hideTooltip(inputKey);
  };

  useEffect(() => {
    if (!errorMessage && inputRef.current!.validationMessage) {
      inputRef.current!.setCustomValidity('');
    }

    if (errorMessage && errorMessage !== prevError) {
      inputRef.current!.setCustomValidity(errorMessage);
      showTooltip(inputRef.current!, inputKey);
    }
  }, [errorMessage, inputKey, prevError, showTooltip]);

  return (
    <React.Fragment>
      <StyledLabel>{`${label}${required ? '*' : ''}`}</StyledLabel>
      <StyledInput
        autoFocus={autoFocus}
        data-tip={errorMessage}
        errorMessage={errorMessage}
        onBlur={handleBlur}
        onChange={handleChange}
        onFocus={handleFocus}
        ref={inputRef}
        required={required}
        touched={touched}
        type={type}
        value={value}
      />
    </React.Fragment>
  );
};

export default memo(InputText);
