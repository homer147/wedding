import React, { memo, useEffect, useRef } from 'react';
import styled from 'styled-components';

import { InputProps, StyledLabel, TStyledInput } from './Input';
import usePrevious from '@hooks/usePrevious';
import { SharedInputStyles } from '@sharedStyles';
import { executeValidationRules, noop, remify } from '@utils';

const StyledTextarea = styled.textarea<TStyledInput>`
  ${SharedInputStyles}
  height: ${remify(100)};

  ${({ errorMessage, theme, touched }) =>
    errorMessage && touched && `box-shadow: inset 0px 0px 0px ${remify(2)} ${theme.colors.error};`}
`;

const InputTextarea = ({
  autoFocus,
  errorMessage,
  hideTooltip = noop,
  inputKey,
  label,
  required,
  setErrorMessage = noop,
  setInputValue = noop,
  showTooltip = noop,
  touched,
  validations = [],
  value = ''
}: InputProps) => {
  const inputRef = useRef<HTMLTextAreaElement>(null);
  const prevError = usePrevious(errorMessage);

  const validate = (e: React.FocusEvent<HTMLTextAreaElement>) => {
    let errorMessage = inputRef.current!.validationMessage || '';

    if (!errorMessage) {
      errorMessage = executeValidationRules(validations, e.target.value) || '';
    }

    if (errorMessage) {
      setErrorMessage(inputKey, errorMessage);
    }
  };

  const handleBlur = (e: React.FocusEvent<HTMLTextAreaElement>) => {
    validate(e);

    if (!touched) {
      setInputValue(inputKey, { touched: true });
    }
  };

  const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setInputValue(inputKey, { value: e.target.value });
  };

  const handleFocus = () => {
    if (errorMessage) {
      setErrorMessage(inputKey, '');
      inputRef.current!.setCustomValidity('');
    }
    hideTooltip(inputKey);
  };

  useEffect(() => {
    if (errorMessage && errorMessage !== prevError) {
      inputRef.current!.setCustomValidity(errorMessage);
      showTooltip(inputRef.current!, inputKey);
    }
  }, [errorMessage, inputKey, prevError, showTooltip]);

  return (
    <React.Fragment>
      <StyledLabel>{`${label}${required ? '*' : ''}`}</StyledLabel>
      <StyledTextarea
        autoFocus={autoFocus}
        data-tip={errorMessage}
        errorMessage={errorMessage}
        onBlur={handleBlur}
        onChange={handleChange}
        onFocus={handleFocus}
        ref={inputRef}
        required={required}
        touched={touched}
        value={value}
      />
    </React.Fragment>
  );
};

export default memo(InputTextarea);
