import { initialState, TInput } from './Form';

export const updateCaptchaAction = 'updateCaptcha';
export const updateErrorAction = 'updateError';
export const updateInputAction = 'updateInput';
export const finaliseCallAction = 'finaliseCall';
export const submitPendingAction = 'submitPending';

export type TAction = {
  type?: string;
  payload?: any;
};

type TErrorMessage = {
  [key: string]: string;
};

export type TFormState = {
  inputs: TInputsState;
  errorMessages: TErrorMessage;
  pending: boolean;
  recaptcha: string | null;
};

export type TInputsState = {
  [key: string]: TInput;
};

export default function reducer(state: TFormState, action: TAction) {
  const { type, payload } = action;

  switch (type) {
    case finaliseCallAction:
      return initialState;

    case submitPendingAction:
      return {
        ...state,
        pending: payload
      };

    case updateCaptchaAction:
      return {
        ...state,
        recaptcha: payload
      };

    case updateErrorAction:
      return {
        ...state,
        errorMessages: {
          ...state.errorMessages,
          [payload.inputKey]: payload.message
        }
      };

    case updateInputAction:
      return {
        ...state,
        inputs: {
          ...state.inputs,
          [payload.inputKey]: {
            ...state.inputs[payload.inputKey],
            ...payload.inputState
          }
        }
      };

    default:
      throw new Error();
  }
}
