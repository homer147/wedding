import { sanitize } from 'dompurify';
import React, { useRef, useState } from 'react';
import { NavLink } from 'react-router-dom';
import styled, { css } from 'styled-components';

import { TItem } from '@components/Header';
import { ItalianaStyle } from '@sharedStyles';
import { disableScroll, enableScroll, remify } from '@utils';

interface HamburgerProps {
  items: TItem[];
}

interface IsOpen {
  isOpen: boolean;
}

const IconContainer = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
  width: 100%;
  padding-left: ${remify(20)};
  z-index: 2;
  background-color: ${({ theme }) => theme.colors.hunterGreen};
  border-bottom: 2px solid ${({ theme }) => theme.colors.silver};
`;

const NavIcon = styled.div<IsOpen>`
  width: 30px;
  height 30px;
  cursor: pointer;

  &:after, &:before, div {
    background-color: ${({ theme }) => theme.colors.ivory};
    border-radius: 3px;
    content: '';
    display: block;
    height: 4px;
    margin: 5px 0;
    transition: all .2s ease-in-out;
  }

  ${({ isOpen }) =>
    isOpen &&
    css`
      &:before {
        transform: translateY(9px) rotate(135deg);
      }

      &:after {
        transform: translateY(-9px) rotate(-135deg);
      }

      div {
        transform: scale(0);
      }
    `}
`;

const Overlay = styled.div<IsOpen>`
  display: ${({ isOpen }) => (isOpen ? 'block' : 'none')};
  width: 100vw;
  height: 100vh;
`;

const NavContainer = styled.div<IsOpen>`
  position: absolute;
  top: ${remify(70)};
  height: calc(100vh - ${remify(70)});
  width: ${({ isOpen }) => (isOpen ? '60vw' : '0')};
  background-color: ${({ theme }) => theme.colors.hunterGreen};
  overflow: hidden;
  transition: width 0.1s linear;

  ${({ isOpen, theme }) =>
    isOpen &&
    css`
      border-right: 2px solid ${theme.colors.silver};
    `};
`;

const Nav = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${remify(10)} 0 0 ${remify(20)};
`;

const NavItem = styled(NavLink)`
  ${ItalianaStyle}
  position: relative;
  align-self: flex-start;
  padding: ${remify(15)} 0;
  font-size: ${remify(24)};
  color: ${({ theme }) => theme.colors.ivory};
  text-decoration: none;
  white-space: nowrap;

  &.active:after {
    content: '';
    position: absolute;
    left: 0;
    bottom: ${remify(6)};
    width: 100%;
    height: ${remify(2)};
    background-color: ${({ theme }) => theme.colors.silver};
  }
`;

const Hamburger = ({ items = [] }: HamburgerProps) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const touchStartPosition = useRef<number>(0);

  const toggleNav = () => {
    isOpen ? enableScroll() : disableScroll();
    setIsOpen(!isOpen);
  };

  const handleOverlayClick = () => {
    toggleNav();
  };

  const handleLinkClick = () => {
    toggleNav();
  };

  const handleTouchStart = (e: React.TouchEvent) => {
    touchStartPosition.current = e.changedTouches[0].screenX;
  };

  const handleTouchEnd = (e: React.TouchEvent) => {
    if (touchStartPosition.current - e.changedTouches[0].screenX > 50) {
      toggleNav();
    }

    touchStartPosition.current = 0;
  };

  return (
    <React.Fragment>
      <IconContainer>
        <NavIcon onClick={toggleNav} isOpen={isOpen}>
          <div></div>
        </NavIcon>
      </IconContainer>
      <Overlay isOpen={isOpen} onClick={handleOverlayClick} />
      <NavContainer isOpen={isOpen} onTouchStart={handleTouchStart} onTouchEnd={handleTouchEnd}>
        <Nav>
          {items.map(({ link, title }: TItem, index: number) => (
            <NavItem
              activeClassName="active"
              dangerouslySetInnerHTML={{ __html: sanitize(title) }}
              exact
              key={index}
              onClick={handleLinkClick}
              to={link}
            />
          ))}
        </Nav>
      </NavContainer>
    </React.Fragment>
  );
};

export default Hamburger;
