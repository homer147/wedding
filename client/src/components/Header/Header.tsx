import React, { useContext } from 'react';
import styled from 'styled-components';

import { Hamburger, Navbar } from '@components';
import { ResponsiveHandlingContext } from '@context/responsiveHandlingContext';
import { BreakpointKeys } from '@constants';
import { remify } from '@utils';

import menuConfig from '@config/menu.json';

export type TItem = {
  link: string;
  title: string;
};

// massive z-index to sit on top of tooltips
const HeaderContainer = styled.div`
  position: fixed;
  height: ${remify(70)};
  width: 100%;
  z-index: 1000;
`;

const Header = () => {
  const { breakpoint } = useContext(ResponsiveHandlingContext);

  return (
    <HeaderContainer>
      {breakpoint === BreakpointKeys.MOBILE ? (
        <Hamburger items={menuConfig.items} />
      ) : (
        <Navbar items={menuConfig.items} />
      )}
    </HeaderContainer>
  );
};

export default Header;
