import React from 'react';
import { mount, render } from 'enzyme';
import 'jest-styled-components';
import { ThemeProvider } from 'styled-components';

import Hero from './Hero';

const image = '/images/mock-image.png';
const theme = {
  colors: {
    silver: '#afb1ae'
  }
};

describe('Hero', () => {
  const defaultProps = {};
  const doWithMode = (mode: (component: JSX.Element) => void) => (
    overrides = {}
  ) =>
    mode(
      <ThemeProvider theme={theme}>
        <Hero image={image} {...defaultProps} {...overrides} />
      </ThemeProvider>
    );

  it('renders without crashing', () => {
    const hero = doWithMode(render)();
    expect(hero).toMatchSnapshot();
  });

  it('applies the correct styling', () => {
    const hero = doWithMode(mount)();
    expect(hero).toHaveStyleRule(
      'border-bottom',
      `2px solid ${theme.colors.silver}`
    );
    expect(hero).toHaveStyleRule(
      'background',
      `url(${image}) no-repeat center`
    );
  });
});
