import React, { memo, useContext } from 'react';
import styled from 'styled-components';

import { ResponsiveHandlingContext } from '@context/responsiveHandlingContext';
import { BreakpointKeys } from '@constants';
import { remify } from '@utils';

interface HeroProps {
  breakpoint?: string;
  image: string;
}

const Banner = styled.div<HeroProps>`
  background: url(${({ image }) => image}) no-repeat center;
  background-size: cover;
  height: ${({ breakpoint }) =>
    breakpoint === BreakpointKeys.DESKTOP ? remify(600) : remify(400)};
  border-bottom: 2px solid ${({ theme }) => theme.colors.silver};
`;

const Hero = ({ image }: HeroProps) => {
  const { breakpoint } = useContext(ResponsiveHandlingContext);

  if (!breakpoint) {
    return null;
  }

  return <Banner breakpoint={breakpoint} image={image} />;
};

export default memo(Hero);
