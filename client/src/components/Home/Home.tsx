import React from 'react';

import image from '@images/kate-michael.jpg';
import { Hero, Rsvp } from '@components';
import { Container, Content, Paragraph } from '@sharedStyles';

const Home = () => {
  return (
    <Container>
      <Hero image={image} />
      <Content>
        <Rsvp />
        <Paragraph>
          It was an historic day in 2018 when we first met. Michael had recently bought his
          telescope and was visiting the observatory where Kate was working and... wait, that wasn't
          it. Ahh, we were out hiking one sunny winter's day when... actually no, that wasn't it
          either. Maybe it was a night at the theatre? Or on an early morning run..?
        </Paragraph>
        <Paragraph>
          Can you even believe it? We have so much in common and do all the same things, but still
          never crossed paths. Thank goodness for the internet! A few messages later and finally we
          did meet at Sculpture by the Sea.
        </Paragraph>
        <Paragraph>
          Some highlights of our adventures since then include spotting a wild platypus, skiing
          Australia's majestic Snowy Mountains (let’s forget about Kate’s ankle), tramping damply
          around New Zealand, stargazing (and producing some of the most amateur astrophotography
          you've ever seen)... and generally increasing the mileage on Michael’s car.
        </Paragraph>
        <Paragraph>
          Now it's time for the next chapter. We are both very excited to be able to share our
          wedding day with our family and friends, and can’t wait to make some more amazing memories
          with you all. It is really sad that many of Kate’s family and friends from overseas won't
          be able to join us, but please keep your eye on our website for details of a possible live
          stream!
        </Paragraph>
        <Paragraph>Don't forget to RSVP and we'll see you on the 8th May!</Paragraph>
      </Content>
    </Container>
  );
};

export default Home;
