import React from 'react';
import styled from 'styled-components';

import keys from '@config/keys.json';
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';
import { remify } from '@utils';

interface MapContainerProps {
  coordinates: google.maps.LatLngLiteral;
}

const Container = styled.div`
  margin-top: ${remify(40)};
`;

const MapContainer = ({ coordinates }: MapContainerProps) => {
  const mapStyles = {
    width: '400px',
    maxWidth: '100%',
    height: '400px',
    margin: '0 auto'
  };

  const defaultCenter = {
    lat: coordinates.lat,
    lng: coordinates.lng
  };

  const openGoogleMaps = () => {
    const url = `https://www.google.com.sa/maps/search/?api=1&query=${coordinates.lat},${coordinates.lng}`;

    window.open(url, '_blank');
  };

  return (
    <Container>
      <LoadScript googleMapsApiKey={keys.mapsAPIKey}>
        <GoogleMap mapContainerStyle={mapStyles} zoom={15} center={defaultCenter}>
          <Marker position={coordinates} onClick={() => openGoogleMaps()} />
        </GoogleMap>
      </LoadScript>
    </Container>
  );
};

export default MapContainer;
