import React, { useContext, useEffect, useRef } from 'react';
import styled from 'styled-components';
import { sanitize } from 'dompurify';

import { CloseButton } from '@components';
import { ModalContext } from '@context/modalContext';
import { PopupContainer, StyledButton } from '@sharedStyles';

interface MessageProps {
  message: string;
  buttonLabel?: string;
}

const MessageContainer = styled(PopupContainer)`
  text-align: center;
`;

const Message = ({ message, buttonLabel = 'Okay' }: MessageProps) => {
  const { closeModal } = useContext(ModalContext);
  const ref = useRef<HTMLButtonElement>(null);

  const handleKeyDown = (e: React.KeyboardEvent) => {
    if (!e.shiftKey && e.key === 'Tab') {
      e.preventDefault();
    }
  };

  useEffect(() => {
    ref.current && ref.current.focus();
  }, []);

  return (
    <MessageContainer>
      <CloseButton onClick={closeModal} />
      <div dangerouslySetInnerHTML={{ __html: sanitize(message) }} />
      <StyledButton onClick={closeModal} onKeyDown={handleKeyDown} ref={ref}>
        {buttonLabel}
      </StyledButton>
    </MessageContainer>
  );
};

export default Message;
