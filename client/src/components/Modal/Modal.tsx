import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';

import { ModalContext } from '@context/modalContext';
import { stopPropagation } from '@utils';

// massive z-index to sit on top of tooltips
const ModalOverlay = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1001;
  background-color: rgba(0, 0, 0, 0.5);
  touch-action: none;
`;

const preventDefault = (e: Event) => e.preventDefault();

const handleKeyPress = (e: KeyboardEvent) => {
  if (e.key === 'ArrowDown' || e.key === 'ArrowUp') {
    preventDefault(e);
  }
};

const Modal = () => {
  const modalDiv = document.getElementById('modal');
  let { modalContent, isModal } = React.useContext(ModalContext);

  useEffect(() => {
    if (isModal) {
      document.body.addEventListener('wheel', preventDefault, {
        passive: false
      });
      document.body.addEventListener('keydown', handleKeyPress);
    } else {
      document.body.removeEventListener('wheel', preventDefault);
      document.body.removeEventListener('keydown', handleKeyPress);
    }
  }, [isModal]);

  if (modalDiv && isModal) {
    return ReactDOM.createPortal(
      <ModalOverlay>
        <div onClick={stopPropagation}>{modalContent}</div>
      </ModalOverlay>,
      modalDiv
    );
  }

  return null;
};

export default Modal;
