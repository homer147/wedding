import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { mount, render } from 'enzyme';
import { ThemeProvider } from 'styled-components';

import Navbar from './Navbar';
import menuData from '@config/menu.json';

const { items } = menuData;
const theme = {
  colors: {
    ivory: '#fffff0',
    silver: '#afb1ae'
  }
};

const convert = (string: string) => string.replace('&amp;', '&');

describe('Navbar', () => {
  const defaultProps = {};
  const doWithMode = (mode: (component: JSX.Element) => any) => (
    overrides = {}
  ) =>
    mode(
      <ThemeProvider theme={theme}>
        <Router>
          <Navbar items={items} {...defaultProps} {...overrides} />
        </Router>
      </ThemeProvider>
    );

  it('renders without crashing', () => {
    const navbar = doWithMode(render)();
    expect(navbar).toMatchSnapshot();
  });

  it('has the correct number of items', () => {
    const navbar = doWithMode(render)();
    expect(navbar.children().length).toEqual(items.length);
  });

  it('renders the correct title', () => {
    const navbar = doWithMode(mount)();
    items.forEach((item, i) => {
      expect(navbar.find('nav').childAt(i).text()).toEqual(
        convert(items[i].title)
      );
      expect(navbar.find('nav').childAt(i).props().to).toEqual(
        convert(items[i].link)
      );
    });
  });
});
