import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';
import { sanitize } from 'dompurify';

import { TItem } from '@components/Header';
import { ItalianaStyle } from '@sharedStyles';
import { remify } from '@utils';

interface NavbarProps {
  items: TItem[];
}

const NavContainer = styled.nav`
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: 100%;
  height: 100%;
  background-color: ${({ theme }) => theme.colors.hunterGreen};
  border-bottom: 2px solid ${({ theme }) => theme.colors.silver};
`;

export const NavItem = styled(NavLink)`
  ${ItalianaStyle}
  position: relative;
  letter-spacing: ${remify(1)};
  font-size: ${remify(20)};
  color: ${({ theme }) => theme.colors.ivory};
  text-decoration: none;

  &.active:after {
    content: '';
    position: absolute;
    left: 0;
    bottom: ${remify(-8)};
    width: 100%;
    height: ${remify(2)};
    background-color: ${({ theme }) => theme.colors.silver};
  }
`;

const Navbar = ({ items = [] }: NavbarProps) => {
  return (
    <NavContainer>
      {items.map(({ link, title }: TItem, index: number) => (
        <NavItem
          activeClassName="active"
          dangerouslySetInnerHTML={{ __html: sanitize(title) }}
          exact
          key={index}
          to={link}
        />
      ))}
    </NavContainer>
  );
};

export default Navbar;
