import React from 'react';

import { Hero, Map } from '@components';
import image from '@images/campbells-stores.jpg';
import { receptionCoords } from '@constants';
import { Container, Content, List, ListItem, Paragraph } from '@sharedStyles';

const Reception = () => {
  return (
    <Container>
      <Hero image={image} />
      <Content>
        <List>
          <ListItem>25 Hickson Road, Dawes Point NSW 2000</ListItem>
          <ListItem>Start Time 6pm</ListItem>
        </List>
        <Paragraph>
          Campbell's Stores is a beautiful, heritage-listed former warehouse building at The Rocks.
          Boasting uncompromised views of the Sydney Harbour Bridge, the Sydney Opera House and
          Sydney Harbour, the stores now have a new life as an innovative dining and entertainment
          destination.
        </Paragraph>
        <Paragraph>
          Named after Robert Campbell, the original stores were built by 1825, and all 11 bays of
          Campbell’s Stores were completed by the early 1860s, with a third storey added by 1887. In
          2019 the Campbell’s Stores underwent a multi-million dollar remediation and restoration
          effort, and is now a world class waterfront dining precinct that celebrates location,
          destination and heritage.
        </Paragraph>
        <Map coordinates={receptionCoords} />
      </Content>
    </Container>
  );
};

export default Reception;
