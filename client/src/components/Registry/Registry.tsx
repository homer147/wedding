import React from 'react';
import styled from 'styled-components';

import { Hero } from '@components';
import image from '@images/gum-leaves.jpg';
import { Container, Content, ContentHeading, Paragraph } from '@sharedStyles';

const CenteredParagraph = styled(Paragraph)`
  text-align: center;
`;

const Registry = () => {
  return (
    <Container>
      <Hero image={image} />
      <Content>
        <ContentHeading>Gifts</ContentHeading>
        <CenteredParagraph>
          We are so grateful to have it all &mdash; family, friends and love galore.
          <br />
          If a gift you intend to feather our nest, but need some help &mdash; may we suggest
          <br />
          Something for our wishing well! We'd love that so much, it would be swell.
        </CenteredParagraph>
        <CenteredParagraph>
          But the most important thing for us is to share this day and night,
          <br />
          Because our wedding day without you there just wouldn't quite be right.
        </CenteredParagraph>
      </Content>
    </Container>
  );
};

export default Registry;
