import React, { useContext, useEffect, useState } from 'react';
import styled from 'styled-components';

import { CloseButton, Form, Message } from '@components';
import { TInput, TInputsState } from '@components/Form';
import { ButtonLabels, callResult } from '@constants';
import rsvpFormData from '@config/rsvpForm.json';
import MessagesData from '@config/messages.json';
import { ModalContext } from '@context/modalContext';
import { submitRsvpCode, submitRsvpInfo } from '@api';
import { ItalianaStyle, PopupContainer, StyledButton } from '@sharedStyles';
import { remify } from '@utils';

interface SecondFormStateProps {
  data: {
    [key: string]: string | number | boolean;
    rowNumber: number;
  };
}

export type SubmitInfoBody = {
  adultsConfirmed?: string;
  childrenConfirmed?: string;
  dietaryRequirements?: string;
  rowNumber: number;
};

export type SubmitCodeBody = string | undefined;

const { firstForm, secondForm }: any = rsvpFormData; // TODO - any

const RsvpButton = styled(StyledButton)`
  padding: ${remify(12)} ${remify(40)};
  font-size: ${({ theme }) => theme.fontSizes.large};
`;

const RsvpContainer = styled.div`
  text-align: center;
  margin: ${remify(40)} 0 ${remify(35)};
`;

const FormTitle = styled.h3`
  ${ItalianaStyle}
  text-align: center;
  margin-top: ${remify(5)};
`;

const parseSecondForm = (secondForm: TInput[], secondFormState: SecondFormStateProps) => {
  const res = secondForm
    .filter((input: TInput) => {
      return input.key && secondFormState.data[input.key] !== null;
    })
    .map((input: TInput) => {
      input.options = [];

      if (input.key && secondFormState.data[input.key]) {
        let i = 0;

        while (i <= secondFormState.data[input.key]) {
          input.options.push(i.toString());
          i += 1;
        }
      }

      return input;
    });

  return res;
};

const FormPopup = () => {
  const [secondFormState, setSecondFormState] = useState<SecondFormStateProps | null>(null);
  const { closeModal, isModal, openModal } = useContext(ModalContext);

  const submitCode = (finaliseCall: (success: boolean) => void) => async (state: TInputsState) => {
    const inputedCode: SubmitCodeBody = state.code?.value;

    if (!inputedCode) {
      console.warn('[Rsvp] No code to submit');
      finaliseCall(callResult.fail);

      return;
    }

    const res = await submitRsvpCode(inputedCode);
    const { errorCode } = res;

    switch (errorCode) {
      case 400:
        finaliseCall(callResult.fail);
        console.warn(`[Rsvp] RSVP code ${inputedCode} is not valid`);
        break;

      case 403:
        finaliseCall(callResult.fail);
        openModal(<Message message={MessagesData.rsvpUsed} />);
        console.warn(`[Rsvp] RSVP has already been recorded for ${inputedCode}`);
        break;

      case 404:
        finaliseCall(callResult.fail);
        openModal(<Message message={MessagesData.noRsvpCode} />);
        console.warn(`[Rsvp] No match found for ${inputedCode}`);
        break;

      case 500:
        finaliseCall(callResult.fail);
        openModal(<Message message={MessagesData.serverError} />);
        console.error(`[Rsvp] Server error while fetching for ${inputedCode}`);
        break;

      default:
        setSecondFormState(res as SecondFormStateProps);
        finaliseCall(callResult.success);
        break;
    }
  };

  const submitInfo = (finaliseCall: (success: boolean) => void) => async (
    state: TInputsState,
    rowNumber: number | undefined
  ) => {
    const { dietaryRequirements, maxAdults, maxChildren } = state;

    if (!maxAdults || (secondFormState?.data.maxChildren && !maxChildren)) {
      finaliseCall(callResult.fail);
      console.warn('[Rsvp] RSVP info is not valid');

      return;
    }

    if (!rowNumber) {
      finaliseCall(callResult.fail);
      openModal(<Message message={MessagesData.serverError} />);
      console.warn('[Rsvp] No rowNumber to update');

      return;
    }

    const body: SubmitInfoBody = {
      adultsConfirmed: maxAdults.value,
      childrenConfirmed: maxChildren?.value,
      dietaryRequirements: dietaryRequirements?.value || '',
      rowNumber
    };

    const res = await submitRsvpInfo(body);

    switch (res.errorCode) {
      case 400:
        finaliseCall(callResult.fail);
        console.warn('[Rsvp] RSVP info is not valid');
        break;

      case 404:
        finaliseCall(callResult.fail);
        openModal(<Message message={MessagesData.serverError} />);
        console.warn('[Rsvp] RSVP rowNumber supplied was not found');
        break;

      case 500:
        finaliseCall(callResult.fail);
        openModal(<Message message={MessagesData.serverError} />);
        console.error(`[Rsvp] ${res.message}`);
        break;

      default:
        finaliseCall(callResult.success);
        openModal(
          <Message
            message={`${
              res.data!.attending ? MessagesData.rsvpAttending : MessagesData.rsvpNotAttending
            }`.replace('{{name}}', res.data!.name as string)}
          />
        );
        break;
    }
  };

  useEffect(() => {
    if (!isModal) {
      setSecondFormState(null);
    }
  }, [isModal]);

  return (
    <PopupContainer>
      <CloseButton onClick={closeModal} />
      <FormTitle>RSVP</FormTitle>
      {secondFormState ? (
        <Form
          inputs={parseSecondForm(secondForm, secondFormState)}
          onSubmit={submitInfo}
          submitLabel={ButtonLabels.SUBMIT}
          rowNumber={secondFormState.data.rowNumber}
        />
      ) : (
        <Form inputs={firstForm} onSubmit={submitCode} submitLabel={ButtonLabels.NEXT} />
      )}
    </PopupContainer>
  );
};

const Rsvp = () => {
  const { openModal } = useContext(ModalContext);

  const openForm = () => {
    openModal(<FormPopup />);
  };

  return (
    <React.Fragment>
      <RsvpContainer>
        <RsvpButton onClick={openForm}>RSVP</RsvpButton>
      </RsvpContainer>
    </React.Fragment>
  );
};

export default Rsvp;
