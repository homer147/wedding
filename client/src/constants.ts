// ENUMS
export enum BreakpointKeys {
  MOBILE = 'mobile',
  TABLET = 'tablet',
  DESKTOP = 'desktop'
}

export enum FontSizes {
  STANDARD = 18
}

export enum Breakpoints {
  // DESKTOP: null,
  TABLET = 1024,
  MOBILE = 767
}

export enum FormConstants {
  EMAIL = 'email',
  NUMBER = 'number',
  SELECT = 'select',
  SUBMIT = 'submit',
  TEXT = 'text',
  TEXTAREA = 'textarea'
}

export enum ButtonLabels {
  NEXT = 'Next',
  SUBMIT = 'Submit'
}

export enum Endpoints {
  GETRSVPDETAILS = 'getRsvpDetails',
  SUBMITCONTACTFORM = 'submitContactForm',
  UPDATERSVPDETAILS = 'updateRsvpDetails'
}

// CONSTANTS
export const Dependencies: {
  [key: string]: string;
} = {
  email: 'your email address',
  phone: 'your phone number'
};

export const callResult = {
  success: true,
  fail: false
};

export const receptionCoords = {
  lat: -33.857033,
  lng: 151.209012
};

export const ceremonyCoords = {
  lat: -33.8689435,
  lng: 151.2127874
};

export const hotelCoords = {
  lat: -33.87867767739732,
  lng: 151.2095322613803
};

export const captchaSiteKey = '6LdAOkEaAAAAALDXBnktdnYqGm5lUXxNm_9B3DTT';
