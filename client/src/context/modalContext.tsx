import React, { createContext } from 'react';

import { Modal } from '@components';
import useModal from '@hooks/useModal';

interface ModalProviderProps {
  children: React.ReactNode;
}

interface InitContextProps {
  closeModal: () => void;
  isModal: boolean;
  modalContent?: React.ReactNode;
  openModal: (content: React.ReactNode) => void;
}

const ModalContext = createContext({} as InitContextProps);
const { Provider } = ModalContext;

const ModalProvider = ({ children }: ModalProviderProps) => {
  const { openModal, closeModal, isModal, modalContent } = useModal();

  return (
    <Provider value={{ isModal, openModal, closeModal, modalContent }}>
      <Modal />
      {children}
    </Provider>
  );
};

export { ModalContext, ModalProvider };
