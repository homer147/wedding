import React, { createContext } from 'react';

import useResponsiveHandling from '@hooks/useResponsiveHandling';

interface ResponsiveHandlingProviderProps {
  children: React.ReactNode;
}

interface InitContextProps {
  breakpoint: string | null;
}

const ResponsiveHandlingContext = createContext({} as InitContextProps);
const { Provider } = ResponsiveHandlingContext;

const ResponsiveHandlingProvider = ({
  children
}: ResponsiveHandlingProviderProps) => {
  const { breakpoint } = useResponsiveHandling();

  return <Provider value={{ breakpoint }}>{children}</Provider>;
};

export { ResponsiveHandlingContext, ResponsiveHandlingProvider };
