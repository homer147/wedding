import { useState } from 'react';

const Modal = () => {
  const [isModal, setIsModal] = useState<boolean>(false);
  const [modalContent, setModalContent] = useState<React.ReactNode>();

  const openModal = (content: React.ReactNode) => {
    setIsModal(true);
    setModalContent(content);
  };

  const closeModal = () => {
    setIsModal(false);
  };

  return { openModal, closeModal, isModal, modalContent };
};

export default Modal;
