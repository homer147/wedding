import { useEffect, useState } from 'react';
import { useMedia } from 'react-media';

import { BreakpointKeys, Breakpoints } from '../constants'; // TODO - alias

interface Dictionary {
  [Key: string]: string;
}

const MEDIA_QUERIES = {
  [BreakpointKeys.MOBILE]: `(max-width: ${Breakpoints.MOBILE}px)`,
  [BreakpointKeys.TABLET]: `(max-width: ${Breakpoints.TABLET}px)`,
  [BreakpointKeys.DESKTOP]: `(min-width: ${Breakpoints.TABLET + 1}px)`
};

const ResponsiveHandling = () => {
  const [breakpoint, setBreakpoint] = useState<string | null>(null);
  const matches = useMedia<Dictionary>({ queries: MEDIA_QUERIES });

  useEffect(() => {
    setBreakpoint(
      Object.keys(matches).find((key) => matches[key] === true) || null
    );
  }, [matches]);

  return { breakpoint };
};

export default ResponsiveHandling;
