import React from 'react';
import ReactDOM from 'react-dom';
import { createGlobalStyle, ThemeProvider } from 'styled-components';

import { App } from '@components';
import { ModalProvider } from '@context/modalContext';
import { ResponsiveHandlingProvider } from '@context/responsiveHandlingContext';
import { FontSizes } from '@constants';

const theme = {
  colors: {
    black: '#000000',
    error: '#ff0033',
    hunterGreen: '#355e3b',
    ivory: '#fffff0',
    silver: '#afb1ae'
  },
  fonts: {
    heading: 'Italiana'
  },
  fontSizes: {
    small: '0.7em',
    medium: '0.8em',
    large: '1.4em'
  }
};

const GlobalStyle = createGlobalStyle`
  body {
    font-family: Montserrat;
    font-weight: 300;
    padding: 0;
    margin: 0;
    font-size: ${FontSizes.STANDARD}px;
    color: ${theme.colors.ivory};
  }
`;

ReactDOM.render(
  <React.StrictMode>
    <GlobalStyle />
    <ThemeProvider theme={theme}>
      <ResponsiveHandlingProvider>
        <ModalProvider>
          <App />
        </ModalProvider>
      </ResponsiveHandlingProvider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
