import styled, { css } from 'styled-components';
import { Close } from '@styled-icons/evaicons-solid';

import { remify } from '@utils';

export const ItalianaStyle = css`
  font-family: ${({ theme }) => theme.fonts.heading};
  letter-spacing: ${remify(1)};
  font-weight: 600;
`;

export const Container = styled.div`
  padding-top: ${remify(70)};
`;

export const Content = styled.div`
  max-width: ${remify(1000)};
  margin: ${remify(30)} auto 0;
`;

export const CloseIcon = styled(Close)`
  width: ${remify(25)};
  cursor: pointer;
`;

export const PopupContainer = styled.div`
  position: relative;
  background-color: ${({ theme }) => theme.colors.hunterGreen};
  border: 1px solid ${({ theme }) => theme.colors.silver};
  padding: ${remify(25)};
`;

export const List = styled.ul`
  list-style-type: none;
  text-align: center;
  padding: 0 ${remify(15)};
  margin-bottom: ${remify(25)};
`;

export const ListItem = styled.li`
  ${ItalianaStyle}
  font-size: ${({ theme }) => theme.fontSizes.large};
  margin: ${remify(10)} 0;
`;

export const Paragraph = styled.p`
  padding: 0 ${remify(15)};
`;

export const ContentHeading = styled.h2`
  ${ItalianaStyle}
  font-size: ${({ theme }) => theme.fontSizes.large};
  text-align: center;
`;

export const SharedInputStyles = css`
  width: 100%;
  box-sizing: border-box;
  padding: ${remify(7)} ${remify(10)};
  border-radius: ${remify(10)};
  margin-bottom: ${remify(25)};
`;

export const StyledButton = styled.button`
  ${ItalianaStyle}
  padding: ${remify(7)} ${remify(20)};
  border-radius: ${remify(10)};
  cursor: pointer;
  font-size: ${remify(20)};
`;
