import { Dependencies, FontSizes } from './constants';
import { TValidation } from '@components/Form';

const preventDefault = (e: Event) => e.preventDefault();

/** Empty function. */
export const noop = (...args: any[]): void => {};

export const stopPropagation = (e: React.MouseEvent) => e.stopPropagation();

export const remify = (px: number, base = FontSizes.STANDARD) => {
  return `${px / base}rem`;
};

const handleKeyPress = (e: KeyboardEvent) => {
  if (e.key === 'ArrowDown' || e.key === 'ArrowUp') {
    preventDefault(e);
  }
};

export const disableScroll = () => {
  document.body.addEventListener('wheel', preventDefault, {
    passive: false
  });
  document.body.addEventListener('keydown', handleKeyPress);
};

export const enableScroll = () => {
  document.body.removeEventListener('wheel', preventDefault);
  document.body.removeEventListener('keydown', handleKeyPress);
};

export const executeValidationRules = (
  validations: TValidation[],
  value: string
) => {
  for (let i = 0; i < validations.length; i++) {
    const { message, rule } = validations[i];
    const pattern = new RegExp(rule);

    if (!pattern.test(value)) {
      return message;
    }
  }
};

export const getDependencyOptions = (key: string, dependencies: string[]) => {
  let str = `${Dependencies[key]}`;

  dependencies.forEach((dependency) => {
    str += ` or ${Dependencies[dependency]}`;
  });

  return str;
};
