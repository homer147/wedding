export const port = 5000;
export const spreadsheetId = "1TAl5kCo_PnPSwcz34PJed391RmqSgLCw12I6wgmF4uI";
export const sheetId = "1006994341";

export const boolMap = {
  FALSE: false,
  TRUE: true,
};

export const codeValidation = "^[0-9]{6}$";

export const webEmailAddress = "mail@kateandmichael.com.au";
export const ourEmailAddress = "kateandmichaelb@gmail.com";
export const recaptchaApi = "https://www.google.com/recaptcha/api/siteverify";
export const certPath = "/etc/letsencrypt/live/kateandmichael.com.au";
