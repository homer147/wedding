import AWS from "aws-sdk";

import { ourEmailAddress, recaptchaApi, webEmailAddress } from "./constants";
import awsCreds from "../aws-creds.json";
import captchaCreds from "../captcha-creds.json";

const { accessKeyId, region, secretAccessKey } = awsCreds;

const SES_CONFIG = {
  accessKeyId,
  secretAccessKey,
  region,
};

const AWS_SES = new AWS.SES(SES_CONFIG);

export const sendRSVPConfirmation = (recipientEmail, templateData) => {
  const params = {
    Source: webEmailAddress,
    Template: "RsvpTemplate",
    Destination: {
      ToAddresses: [recipientEmail],
    },
    TemplateData: JSON.stringify(templateData),
  };

  AWS_SES.sendTemplatedEmail(params).promise();
};

export const submitContactForm = async (req, res) => {
  const {
    captchaCode,
    message,
    senderEmailAddress,
    senderName,
    senderPhone,
  } = req.body.formData;

  if (captchaCode === null) {
    res.status(400).json({
      errorCode: 400,
      message: "Possibly a robot",
    });

    return;
  }

  const isHuman = await fetch(recaptchaApi, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
    },
    body: `secret=${captchaCreds.secretKey}&response=${captchaCode}`,
  })
    .then((res) => res.json())
    .then((json) => json.success)
    .catch((err) => {
      res.status(500).json({
        error: 500,
        errorMessage: `Error in Google Siteverify API. ${err.message}`,
      });
      throw new Error(`Error in Google Siteverify API. ${err.message}`);
    });

  if (!isHuman) {
    res.status(400).json({
      errorCode: 400,
      message: "Possibly a robot",
    });

    return;
  }

  const params = {
    Source: webEmailAddress,
    Destination: {
      ToAddresses: [ourEmailAddress],
    },
    Message: {
      Body: {
        Html: {
          Charset: "UTF-8",
          Data: `<p>${senderName}${
            senderEmailAddress ? ` - ${senderEmailAddress}` : ""
          }${senderPhone ? ` - ${senderPhone}` : ""}</p><p>${message}</p>`,
        },
      },
      Subject: {
        Charset: "UTF-8",
        Data: "Website enquiry",
      },
    },
  };

  if (senderEmailAddress) {
    params.ReplyToAddresses = [senderEmailAddress];
  }

  const result = await AWS_SES.sendEmail(params).promise();

  if (!result.MessageId) {
    res.status(500).json({
      errorCode: 500,
      message: "The email failed to send",
    });

    return;
  }

  res.status(200).json({
    message: "The email sent successfully",
  });
};
