import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import "es6-promise/auto";
import "isomorphic-fetch";

import { submitContactForm } from "./contact";
import { getRsvpDetails, updateRsvpDetails } from "./rsvp";
import { port } from "./constants";

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post("/api/getRsvpDetails", getRsvpDetails);
app.post("/api/updateRsvpDetails", updateRsvpDetails);
app.post("/api/submitContactForm", submitContactForm);

app.listen(port, () => {
  console.log(`server started at http://localhost:${port}`);
});
