import { GoogleSpreadsheet } from "google-spreadsheet";

import { boolMap, codeValidation, sheetId, spreadsheetId } from "./constants";
import { sendRSVPConfirmation } from './contact';

const doc = new GoogleSpreadsheet(spreadsheetId);

export const getRsvpDetails = async (req, res) => {
  const { code } = req.body;
  const pattern = new RegExp(codeValidation);

  if (!pattern.test(code)) {
    res.status(400).send(`${code} failed validation`);
    return;
  }

  await doc.useServiceAccountAuth(require("../creds-from-google.json"));
  await doc.loadInfo();

  const sheet = doc.sheetsById[sheetId];
  const rows = await sheet.getRows();
  const match = rows.find((row) => row.code === code);

  if (!match) {
    res.status(404).json({
      error: 404,
      errorMessage: `There are no codes that match ${code}`,
    });
    return;
  }

  const {
    maxAdults = 1,
    maxChildren = 0,
    conAdults,
    conChildren,
    dietaryRequirements,
    RSVPd,
    _rowNumber,
  } = match;

  if (!maxAdults) {
    res.status(500).json({
      error: 500,
      errorMessage: `There are no maxAdults for ${code}`,
    });
    return;
  }

  if (RSVPd === "TRUE") {
    res.status(403).json({
      error: 403,
      errorMessage: `RSVP has already been recorded for ${code}`,
    });
    return;
  }

  const result = {
    code,
    maxAdults: parseInt(maxAdults, 10),
    maxChildren: parseInt(maxChildren, 10),
    conAdults: parseInt(conAdults, 10),
    conChildren: parseInt(conChildren, 10),
    dietaryRequirements,
    rsvpd: boolMap[RSVPd],
    rowNumber: parseInt(_rowNumber, 10),
  };

  res.status(200).json({ data: result });
};

export const updateRsvpDetails = async (req, res) => {
  const {
    adultsConfirmed,
    childrenConfirmed,
    dietaryRequirements,
    rowNumber,
  } = req.body.info;
  // index = rowNumber - 2
  // minus 1 for index, and minus 1 for header row
  const index = rowNumber - 2;

  if (isNaN(adultsConfirmed)) {
    res.status(400).send("The provided details have failed validation");
    return;
  }

  await doc.useServiceAccountAuth(require("../creds-from-google.json"));
  await doc.loadInfo();

  const sheet = doc.sheetsById[sheetId];
  const rows = await sheet.getRows();

  if (rows[index].maxChildren && isNaN(childrenConfirmed)) {
    res.status(400).send("The provided details have failed validation");
    return;
  }

  if (!rows[index]) {
    res.status(404).json({
      error: 404,
      errorMessage: "The specified row was not found",
    });
    return;
  }

  rows[index].adultsConfirmed = adultsConfirmed;
  rows[index].RSVPd = boolMap.TRUE;
  rows[index].dietaryRequirements = dietaryRequirements;

  if (childrenConfirmed) {
    rows[index].childrenConfirmed = childrenConfirmed;
  }

  await rows[index].save();

  const emailParams= {
    name: rows[index].name,
    dietaryRequirements,
  }

  console.log(adultsConfirmed);

  if (parseInt(adultsConfirmed) === 0) {
    emailParams.adultsConfirmed = adultsConfirmed
  } else if (adultsConfirmed > 1) {
    emailParams.attending = true;
    emailParams.adultsConfirmed = adultsConfirmed
  } else {
    emailParams.attending = true;
    emailParams.adultConfirmed = adultsConfirmed
  }

  if (childrenConfirmed) {
    if (childrenConfirmed > 1) {
      emailParams.childrenConfirmed = childrenConfirmed
    } else {
      emailParams.childConfirmed = childrenConfirmed
    }
  }

  sendRSVPConfirmation(rows[index].email, emailParams);

  res
    .status(200)
    .json({ data: { name: rows[index].name, attending: adultsConfirmed > 0 } });
};
